using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class HandheldItemBase : MonoBehaviour
{
    public abstract void Use();
}
