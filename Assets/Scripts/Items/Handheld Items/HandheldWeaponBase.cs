using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HandheldWeaponBase : HandheldItemBase
{
    public override void Use()
    {
        Attack();
    }

    public abstract void Attack();
    public abstract void AltAttack();
}
