using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemAnimation : MonoBehaviour
{
    public Animator animator;
    public abstract void Play();
}
