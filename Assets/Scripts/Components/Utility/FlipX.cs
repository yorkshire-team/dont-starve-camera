using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipX : MonoBehaviour
{
   public int direction = 1;
   public void Flip(int direction)
   {
       float scale = transform.localScale.y;
       transform.localScale = new Vector3(scale * direction, scale, 1);
   }
}
