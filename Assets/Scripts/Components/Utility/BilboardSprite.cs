using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BilboardSprite : MonoBehaviour
{    
    public GameObject objectToRotate;
    public bool runInEditor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Application.IsPlaying(gameObject))
        {
            var rotX = Camera.main.transform.rotation.x;
            var rotY = Camera.main.transform.rotation.y + objectToRotate.transform.rotation.y;
            var rotZ = Camera.main.transform.rotation.z + objectToRotate.transform.rotation.z;
            Quaternion newRot = new Quaternion(rotX, rotY, rotZ, 1);
            objectToRotate.transform.rotation = newRot;
        }
        else
        {
            if (runInEditor)
            {
                //TurnToCamera(Camera.main);
            }
        }
    }

    void TurnToCamera(Camera camera)
    {
        var rotX = camera.transform.rotation.x;
        var rotY = transform.rotation.y;
        var rotZ = camera.transform.rotation.z + transform.rotation.z;
        Quaternion newRot = new Quaternion(rotX, rotY, rotZ, 1);
        objectToRotate.transform.rotation = newRot;
    }
}
