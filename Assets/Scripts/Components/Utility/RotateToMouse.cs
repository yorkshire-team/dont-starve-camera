using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateToMouse : MonoBehaviour
{
    public enum Axis{X, Y, Z};
    public Axis rotationAxis;
    private Vector2 mousePosition;
    public float offset;
    void Update()
    {
        DoRotation();
    }

    private void DoRotation()
    {
         Vector3 newMouse = new Vector3 (mousePosition.x, mousePosition.y,0);
        float objectDepthFromCamera = Vector3.Dot(
            transform.position - Camera.main.transform.position, 
            Camera.main.transform.forward); 
        Vector3 cursorWorldPosition = Camera.main.ScreenToWorldPoint(newMouse
            + Vector3.forward * objectDepthFromCamera);
            Vector3 localUpNeeded = Vector3.Cross(Vector3.up, 
            cursorWorldPosition - transform.position);
        transform.rotation = Quaternion.LookRotation(Vector3.up, localUpNeeded);
    }

    public void OnMousePoint(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            mousePosition = context.ReadValue<Vector2>();
        }
    }

    public void DRotation()
    {
        Vector3 newMouse = new Vector3 (mousePosition.x, mousePosition.y,0);
        float objectDepthFromCamera = Vector3.Dot(
            transform.position - Camera.main.transform.position, 
            Camera.main.transform.forward); 
        Vector3 cursorWorldPosition = Camera.main.ScreenToWorldPoint(newMouse
            + Vector3.forward * objectDepthFromCamera);
            Vector3 localUpNeeded = Vector3.Cross(Vector3.forward, 
            cursorWorldPosition - transform.position);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, localUpNeeded);
    }
}
