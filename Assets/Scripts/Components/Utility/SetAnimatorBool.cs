using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimatorBool : MonoBehaviour
{
    public Animator animator;
    public string boolName;

    public void Set(bool valueToSet)
    {
        animator.SetBool(boolName, valueToSet);
    }
}
