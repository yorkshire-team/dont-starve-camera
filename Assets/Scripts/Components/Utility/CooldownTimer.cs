using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CooldownTimer : MonoBehaviour
{
    [SerializeField] private float duration = 1f;
    [SerializeField] private UnityEvent onTimerEnd = new UnityEvent();

    private bool running = false;

    public IEnumerator StartTimer()
    {
        running = true;
        yield return new WaitForSeconds(duration);
        onTimerEnd?.Invoke();
        running = false;
    }

    public void Cooldown()
    {
        if (!running)
        {
            StartCoroutine(StartTimer());
        }
    }
}
