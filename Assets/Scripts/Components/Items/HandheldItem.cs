using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HandheldItem : HandheldItemBase
{
    public UnityEvent onUse = new UnityEvent();
    public override void Use()
    {
        //animator.SetTrigger("use");
        onUse?.Invoke();
    }
}
