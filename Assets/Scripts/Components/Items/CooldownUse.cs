using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CooldownUse : MonoBehaviour
{
    public float duration = 1f;
    public UnityEvent OnUse = new UnityEvent();
    private bool onCooldown = false;
    
    private IEnumerator Delay()
    {
        OnUse?.Invoke();
        onCooldown = true;
        yield return new WaitForSeconds(duration);
        onCooldown = false;
    }

    public void Use()
    {
        if (!onCooldown)
        {
            StartCoroutine(Delay());
        }
    }
}
