using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsWeapon : MonoBehaviour
{
    public Transform attackPoint;
    public float attackRadius;
    public LayerMask attackLayerMask;
    private bool canAttack = true;
    public void OnAttack()
    {
        if (canAttack)
        {
            Collider[] hitColliders = Physics.OverlapSphere(attackPoint.position, attackRadius, attackLayerMask);
            foreach (var hitCollider in hitColliders)
            {
                Debug.Log("got hit!");
            }
            //canAttack = false;
        }
    }

    public void AllowAttacking()
    {
        canAttack = true;
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }
}
