using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Animator animator;
    public FlipX flipX;
    public Rigidbody rigidBody;
    public Transform attackPoint;
    public float attackRadius;
    public LayerMask attackLayerMask;

    void Start()
    {

    }
    void Update()
    {
        if (rigidBody.velocity.x < 0)
        {
            flipX.Flip(-1);

        }
        else if (rigidBody.velocity.x > 0)
        {
            flipX.Flip(1);
        }
    }
    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }
}
