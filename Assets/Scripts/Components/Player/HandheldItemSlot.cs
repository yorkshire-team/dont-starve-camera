using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HandheldItemSlot : MonoBehaviour
{
    private HandheldItemBase itemInSlot;

    void Start()
    {
        itemInSlot = GetComponentInChildren<HandheldItemBase>();
    }

    public void PlayerAction(InputAction.CallbackContext context)
    {
       if (context.performed)
       {
           if (itemInSlot != null)
           {
               itemInSlot.Use();
           }
       }
       
    }
}
