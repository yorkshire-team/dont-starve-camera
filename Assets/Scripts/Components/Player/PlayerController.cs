using System.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerController : MonoBehaviour
{
    public Rigidbody playerBody;
    public Animator animator;
    public FlipX flipX;
    public Transform attackPoint;
    public float speed;
    private Vector2 playerInput;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
      // playerBody.AddForce(new Vector3(10f,0f,1f),ForceMode.Force);
        if (playerInput.magnitude <= 0)
        {
            animator.SetBool("running", false);
        }

        if (playerInput.x < 0)
        {
            flipX.Flip(-1);
            //float scale = transform.localScale.y;
            //transform.localScale = new Vector3(-scale, scale, 1);
        }
        else if (playerInput.x > 0)
        {
            flipX.Flip(1);
            //float scale = transform.localScale.y;
            //transform.localScale = new Vector3(scale, scale, 1);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float velX = playerInput.x;
        float velZ = playerInput.y;
        Vector3 move = new Vector3(velX, 0, velZ);
        playerBody.velocity = move * speed;
    }

    public void OnMove( InputAction.CallbackContext context)
    {
        var contextInput = context.ReadValue<Vector2>();
        //playerInput = new Vector2(Mathf.RoundToInt(contextInput.x), Mathf.RoundToInt(contextInput.y));
        playerInput = contextInput;
        if (context.performed)
        {
            animator.SetBool("running", true);
        }
    }
}
